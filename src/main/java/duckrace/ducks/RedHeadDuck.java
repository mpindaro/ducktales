package duckrace.ducks;

import duckrace.Duck;
import duckrace.behaviors.*;

public class RedHeadDuck implements Duck {

    private final QuackBehavior VERSO=new Quack();
    private FlyBehavior fly= new AlmostFlyng();



    @Override
    public String performQuack() {
        return VERSO.quack();
    }

    @Override
    public String performFly() {
        return fly.fly();
    }
}
