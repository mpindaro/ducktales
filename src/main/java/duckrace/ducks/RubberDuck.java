package duckrace.ducks;

import duckrace.Duck;
import duckrace.behaviors.*;

public class RubberDuck implements Duck {

    private final QuackBehavior VERSO=new Squick();
    private FlyBehavior fly= new NotFlying();




    @Override
    public String performQuack() {
        return VERSO.quack();
    }

    @Override
    public String performFly() {
        return fly.fly();
    }
}
