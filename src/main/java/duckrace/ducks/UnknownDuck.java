package duckrace.ducks;

import duckrace.Duck;

public class UnknownDuck implements Duck {

    private static final String VERSO= "Not clear...";

    @Override
    public String performQuack() {
        return VERSO;
    }

    @Override
    public String performFly() {
        return "Can't tell..";
    }
}
