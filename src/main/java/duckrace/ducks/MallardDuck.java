package duckrace.ducks;

import duckrace.Duck;
import duckrace.behaviors.FlyBehavior;
import duckrace.behaviors.Flying;
import duckrace.behaviors.Quack;
import duckrace.behaviors.QuackBehavior;

public class MallardDuck implements Duck {


    private final QuackBehavior VERSO=new Quack();
    private FlyBehavior fly= new Flying();

    @Override
    public String performQuack() {
        return VERSO.quack();
    }

    @Override
    public String performFly() {
        return fly.fly();
    }
}
