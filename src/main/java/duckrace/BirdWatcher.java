package duckrace;

import duckrace.ducks.UnknownDuck;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BirdWatcher {

    List<Duck> pictures = new ArrayList<>();

    String name;
    public BirdWatcher(String name) {

        this.name=name;
    }

    public boolean makePhoto(Duck duck){
        if(new Random().nextBoolean()){
            pictures.add(duck);
            return true;
        }else{
            pictures.add(new UnknownDuck());
            return false;
        }
    }

    public StringBuilder showPhotos() {
        StringBuilder s= new StringBuilder(name + " ha scattato delle foto a: \n");
        for (Duck picture : pictures) {
            s.append(picture.getClass().getSimpleName() + ": "+picture.performQuack() + " " + picture.performFly() + "\n");
        }

        return s;
    }
}
