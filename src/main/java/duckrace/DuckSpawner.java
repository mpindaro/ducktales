package duckrace;

import duckrace.ducks.RedHeadDuck;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DuckSpawner implements Iterator<Duck> {


    private final List<Duck> ducks;
    public int i;

    public DuckSpawner(List<Duck> l){
        assert l!=null;
        ducks=l;
        i=0;
    }

    @Override
    public boolean hasNext() {
        return (i<ducks.size());
    }

    @Override
    public Duck next() {
        assert hasNext();
        return ducks.get(i++);

    }

}
