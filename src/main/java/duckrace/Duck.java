package duckrace;

public interface Duck {

    String performQuack();
    String performFly();

}
