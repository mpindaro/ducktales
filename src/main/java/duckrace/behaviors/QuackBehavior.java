package duckrace.behaviors;

public interface QuackBehavior {
    String quack();
}
