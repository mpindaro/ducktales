package duckrace.behaviors;

public class AlmostFlyng implements FlyBehavior {
    @Override
    public String fly() {
        return "Running very fast";
    }
}
