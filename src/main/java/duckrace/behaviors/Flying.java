package duckrace.behaviors;

public class Flying implements FlyBehavior {
    @Override
    public String fly() {
        return "Flying...";
    }
}
