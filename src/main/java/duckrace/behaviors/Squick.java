package duckrace.behaviors;

public class Squick implements QuackBehavior {
    @Override
    public String quack() {
        return "SQUICK";
    }
}
