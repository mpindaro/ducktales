package duckrace.behaviors;

public interface FlyBehavior {
    String fly();
}
