package duckrace.behaviors;

public class NotFlying implements FlyBehavior {
    @Override
    public String fly() {
        return "not flying";
    }
}
