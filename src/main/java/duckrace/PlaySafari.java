package duckrace;

public class PlaySafari {

    public static void main(String[] args){
        Safari safari = new Safari();
        safari.popola();
        safari.registraPartecipante(new BirdWatcher("Manuel Dileo"));
        safari.osservazione();
        System.out.println(safari.termineOsservazione());
    }


}
