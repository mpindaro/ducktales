package duckrace;

import duckrace.ducks.MallardDuck;
import duckrace.ducks.RedHeadDuck;
import duckrace.ducks.RubberDuck;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Safari implements Iterable<BirdWatcher> {

    private List<Duck> papere = new ArrayList<>();
    private List<BirdWatcher> osservatori= new ArrayList<>();
    private DuckSpawner spawner;

    public void popola() {

        papere=new ArrayList<>();
        int nducks=new Random().nextInt(20)+1;
        for (int i = 0; i < nducks; i++) {
            int ducktype=new Random().nextInt(3);
            switch (ducktype){
                case 0: papere.add(new MallardDuck());
                    break;
                case 1: papere.add(new RedHeadDuck());
                    break;
                case 2: papere.add(new RubberDuck());
                    break;
            }
        }
        spawner= new DuckSpawner(papere);
        assert papere.size()!=0;
    }

    public void osservazione(){

        for (BirdWatcher birdWatcher : osservatori) {
            while (spawner.hasNext()){
                Duck duck= spawner.next();
                birdWatcher.makePhoto(duck);
            }
        }


    }



    public void registraPartecipante(BirdWatcher b) {
        osservatori.add(b);
    }

    @Override
    public Iterator<BirdWatcher> iterator() {
        return osservatori.iterator();
    }

    public String termineOsservazione() {
        StringBuilder s= new StringBuilder();
        for (BirdWatcher birdWatcher :osservatori) {
            s.append(birdWatcher.showPhotos() + "\n");
        }
        return s.toString();
    }
}
