﻿# Duck Tales
Si implementi una simulazione dell'osservazione di papere all'interno di un Safari.
In particolar modo, il sistema deve modellare:
- Un safari popolato da papere.
- Gli osservatori, che partecipano al Safari, con la possibilità di fotografare le papere.
## Ducks
Il safari è popolato dalle seguenti specie di papere:
- RedHeadDuck, il cui verso è quack, con capacità di volare.
- MallardDuck, il cui verso è quack, le quali cercano di volare camminando molto velocemente.
- RubberDuck, il cui verso è squeeze, che, per ovvi motivi, non sono in grado di volare.

Tutte le papere devono implementare l'interfaccia Duck fornita.
## Osservazione
Fino a quando ci sono papere all'interno del Safari, esse appaiono agli Osservatori.
Gli osservatori fotografano le papere viste e le salvano con due possibili esiti:
- La foto è nitida e l'osservatore è in grado di riconoscere la papera e di memorizzarsene il tipo.
- La foto è mossa o sfuocata e l'osservatore non riesce a riconoscere la papera ; in questo caso, all'interno della sua collezione di papere viste, memorizza un UnknownDuck.
## Popolazione del Safari
Il Safari viene popolato prima dell'osservazione con un numero arbitrario di papere generate randomicamente.
## Spawn
Si utilizzi un iterator per l'apparizione della prossima papera.
## Termine dell'osservazione
Al termine dell'osservazione, gli osservatori mostrano in ordine i propri risultati specificando:
- Il proprio nome.
- La specie e il verso di ogni papera riconosciuta.
- Un'indicazione che simboleggi ogni papera non riconosciuta.
## Note
Per facilitare la comprensione del sistema, viene fornita la classe PlaySafari che contiene un main con il quale il sistema deve compilare.
> Written by Manuel Dileo.
